# Make a zipfile including all the files needed to flash the abox_edge

ifeq ($(TARGET_DEVICE), abox_edge)

abox_name := $(TARGET_PRODUCT)
ifeq ($(TARGET_BUILD_TYPE), debug)
  abox_name := $(abox_name)_debug
endif
abox_name := $(abox_name)-flashall-$(FILE_NAME_TAG)

ABOX_ZIP        := $(TARGET_OUT_INTERMEDIATES)/$(abox_name).zip
ABOX_VENDOR     := vendor/bsp/marvell/device/abox_edge
ABOX_BOOTLOADER := $(ABOX_VENDOR)/bootloader
ABOX_GPT        := $(ABOX_VENDOR)/bin

ABOX_FLASHFILES := $(INSTALLED_BOOTIMAGE_TARGET)
ABOX_FLASHFILES += $(INSTALLED_SYSTEMIMAGE)
ABOX_FLASHFILES += $(INSTALLED_USERDATAIMAGE_TARGET)
ABOX_FLASHFILES += $(INSTALLED_CACHEIMAGE_TARGET)
ABOX_FLASHFILES += $(INSTALLED_RECOVERYIMAGE_TARGET)

ABOX_FLASHFILES += $(ABOX_BOOTLOADER)/obm.bin \
                   $(ABOX_BOOTLOADER)/u-boot.bin \
                   $(ABOX_BOOTLOADER)/tee_tw.bin \
                   $(ABOX_BOOTLOADER)/teesst.img

ABOX_FLASHFILES += $(ABOX_GPT)/misc.bin \
                   $(ABOX_GPT)/primary_gpt \
                   $(ABOX_GPT)/secondary_gpt

$(ABOX_ZIP): $(ABOX_FLASHFILES)
	$(hide) echo "Package flashfiles: $@"
	$(hide) rm -rf $@
	$(hide) mkdir -p $(dir $@)
	$(hide) zip -j $@ $(ABOX_FLASHFILES)

$(call dist-for-goals, dist_files, $(ABOX_ZIP))

endif
